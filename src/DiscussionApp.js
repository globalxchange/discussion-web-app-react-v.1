import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import "./static/scss/master.scss";
function DiscussionApp() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
}

export default DiscussionApp;
