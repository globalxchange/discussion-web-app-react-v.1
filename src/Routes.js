import { Route, Switch } from "react-router";
import GetMobileAppPage from "./pages/GetMobileAppPage";
import HomePage from "./pages/HomePage";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route exact path="/getapp" component={GetMobileAppPage} />
    </Switch>
  );
}

export default Routes;
