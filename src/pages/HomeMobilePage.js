import React from "react";
import MainLayout from "../layouts/ChatLayout/MainLayout";
import logoIcon from "../static/images/logos/mainIcon.svg";
import discussionText from "../static/images/logos/discussionText.svg";
import fireWhite from "../static/images/clipIcons/fireWhite.svg";
import talkWhite from "../static/images/clipIcons/talkWhite.svg";
import mobileIcon from "../static/images/clipIcons/mobileIcon.svg";
import { Link } from "react-router-dom";

function HomeMobile() {
  return (
    <MainLayout className="homeMobile">
      <div className="head">
        <img src={logoIcon} alt="" />
        <img className="title" alt="" src={discussionText} />
      </div>
      <div className="btns">
        <div className="btnIdea">
          <img src={fireWhite} alt="" /> Ideas Market
        </div>
        <div className="btnTalk">
          <img src={talkWhite} alt="" /> Start Talking
        </div>
      </div>
      <Link to="/getapp" className="footer">
        <img src={mobileIcon} alt="" />
        Download Mobile App
      </Link>
    </MainLayout>
  );
}

export default HomeMobile;
