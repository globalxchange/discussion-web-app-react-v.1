import React, { Fragment, useContext, useState } from "react";
import HomeCarouselComponent from "../components/HomeCarouselComponent/HomeCarouselComponent";
import { ChatsContext } from "../context/ChatsContext";
import MainLayout from "../layouts/ChatLayout/MainLayout";
import MainNavbar from "../layouts/MainNavbar/MainNavbar";

import logo from "../static/images/logos/mainLogo.svg";
import streamIcon from "../static/images/logos/streamIcon.svg";
import fireWhite from "../static/images/clipIcons/fireWhite.svg";
import talkWhite from "../static/images/clipIcons/talkWhite.svg";
import bgBusiness from "../static/images/bgs/bgBusiness.jpg";
import bgPolitics from "../static/images/bgs/bgPolitics.jpg";
import bgSports from "../static/images/bgs/bgSports.jpg";
import useWindowDimensions from "../utils/WindowSize";
import HomeMobile from "./HomeMobilePage";

function HomePage() {
  const { chatOn, setChatOn } = useContext(ChatsContext);
  const { width } = useWindowDimensions();

  const [list, setList] = useState([
    {
      key: 1,
      bg: bgPolitics,
      title: "About Politics",
      desc:
        "Legal Functions Absorb Uneccessary Time And Capital From Businesses. Not Anymore! The InstaLegal App Is Like Having  Your Very Own Legal Department Without The Cost.",
    },
    {
      key: 2,
      bg: bgBusiness,
      title: "About Business",
      desc:
        "Legal Functions Absorb Uneccessary Time And Capital From Businesses. Not Anymore! The InstaLegal App Is Like Having  Your Very Own Legal Department Without The Cost.",
    },
    {
      key: 3,
      bg: bgSports,
      title: "About Sports",
      desc:
        "Legal Functions Absorb Uneccessary Time And Capital From Businesses. Not Anymore! The InstaLegal App Is Like Having  Your Very Own Legal Department Without The Cost.",
    },
  ]);

  return (
    <Fragment>
      {width > 768 ? (
        <MainLayout className="homePage">
          <MainNavbar
            className=""
            logo={logo}
            onLogoClick={() => {}}
            chatOn={chatOn}
            setChatOn={setChatOn}
            btIcon={streamIcon}
            onBtClick={() => {}}
            btLabel="Streaming Apps"
          />
          <div className="homeView">
            <div className="content">
              <div className="titleOne">Listen To Discussions</div>
              <div className="titleTwo">{list[1]?.title}</div>
              <p className="desc">{list[1]?.desc}</p>
              <div className="btns">
                <div className="btnIdea">
                  <img src={fireWhite} alt="" /> Ideas Market
                </div>
                <div className="btnTalk">
                  <img src={talkWhite} alt="" /> Start Talking
                </div>
              </div>
            </div>
            <HomeCarouselComponent list={list} setList={setList} />
          </div>
        </MainLayout>
      ) : (
        <HomeMobile />
      )}
    </Fragment>
  );
}

export default HomePage;
